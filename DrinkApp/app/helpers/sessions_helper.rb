module SessionsHelper
def log_in(user)
  session[:user_id] = user.id  #this places temporary cookie on the user browser containing an encrypted version of the  
end                            #user's id, which allows us to retrieve the id on subsequent pages   

def current_user
  @current_user ||= User.find_by(id: session[:user_id])
end

def logged_in?                  #return true if user is logged in ,fakse otherwise
  !current_user.nil?
end

def log_out
	session.delete(:user_id)
	@current_user = nil
end
end
