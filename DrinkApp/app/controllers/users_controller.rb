class UsersController < ApplicationController

def index
@user = User.all
end

def new
@user = User.new
end

def create
@user = User.new(allowed_params)

if(@user.save)
redirect_to users_path
else 
render 'new'
end

end
def show
@user = User.find(params[:id])

end

def edit
#will have template
@user = User.find(params[:id])
end

def update
#will update and render
@user = User.find(params[:id])
if @user.update_attributes(allowed_params)
redirect_to drinks_path
else 
render 'edit'
end

end


def destroy
#will destroy and render
@user = User.find(params[:id])
@user.destroy
redirect_to users_path
end

private
def allowed_params
 params.required(:user).permit(:name,:email,:passsword)
end
end
