class DrinksController < ApplicationController

#ssl_required :create,:edit,:update
def index
#with template
@drink = Drink.all
end

def show
#with template
@drink = Drink.find(params[:id])

end

def new
#with template
@drink = Drink.new
end

def create
#will save and render
@drink = Drink.new(allowed_params)

if(@drink.save)
redirect_to drinks_path
else 
render 'new'
end

end

def edit
#will have template
@drink = Drink.find(params[:id])
end

def update
#will update and render
@drink = Drink.find(params[:id])
if @drink.update_attributes(allowed_params)
redirect_to drinks_path
else 
render 'edit'
end

end

def destroy
#will destroy and render
@drink = Drink.find(params[:id])
@drink.destroy
redirect_to drinks_path

end

private 
def allowed_params
 params.required(:drink).permit(:name,:size)
end 


end
