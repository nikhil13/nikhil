class UrlsController < ApplicationController
  def new
    @url = Url.new
  end

  def create
    @url = Url.new(user_params)
    if @url.save
      flash[:shortened_id] = @url.id
      redirect_to new_url_url
    else
      render :action => "new"
    end
  end

  def show
    @url = Url.find(params[:id])
    redirect_to @url.url
  end

 def user_params
    params.require(:url).permit(:url)
  end

end
