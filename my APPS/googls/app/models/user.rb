class User < ActiveRecord::Base
  def self.from_omniauth(auth)
    where(client: auth.client, uid: auth.uid).first_or_initialize.tap do |user|
      user.client = auth.client
      client.authorize_url
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      client.request_access_token(params["code"])
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end 
end
