class User
 	include MongoMapper::Document

	key :name, String, :required => true
	key :email, String
	many :microposts
end
