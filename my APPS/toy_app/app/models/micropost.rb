class Micropost
	include MongoMapper::Document
	key :content, String
	key :user_id, ObjectId
	belongs_to :users

end
