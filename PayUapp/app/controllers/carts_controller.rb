class CartsController < ApplicationController

  def index
    @carts = Cart.all
  end

  def show        
    @cart = current_cart
  end

  def new
    @cart = Cart.new
  end

  def edit
    @cart = Cart.find(params[:id])
  end

  def create
    @cart = Cart.new(params[:cart])
  end

  def update
    @cart = Cart.find(params[:id])
  end

  def destroy
    @cart = Cart.find(params[:id])
    @cart.destroy
  end

  def payu_return

	params["pg"]="cc"
        params["bankcode"]="cc"
	params["ccnum"]="4591510123996519"
        params["ccvv"]="179"
        params["ccname"]="nikhil kumar"
    notification = PayuIndia::Notification.new(request.query_string, options = {:key => 'gtKFFx', :salt => 'eCwWELxi', :params => params})

    @cart = Cart.find(notification.invoice) # notification.invoice is order id/cart id which you have submited from payment direction page.

    if notification.acknowledge      
      begin
        if notification.complete?          
          @cart.status = 'success'
          @cart.purchased_at = Time.now
          @order = Order.create(:total => notification.gross, :card_holder_name => params['ccname'], :pg => params['cc'], :bankcode => params['cc'], :ccnum => params['4591510123996519'], :ccvv => params['179'], :order_number => notification.invoice)
          reset_session
          redirect_to @order
        else          
          @cart.status = "failed"
          render :text => "Order Failed! #{notification.message}"
        end
      ensure
        @cart.save
      end
    end    
  end
end
